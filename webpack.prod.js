const merge = require("webpack-merge");
const config = require("./webpack.config");
const path = require("path");

module.exports = merge(config, {
  entry: {
    main: path.resolve(__dirname, "assets/src/index.js"),
    UIassets: path.resolve(
      __dirname,
      "assets/src/components/posts/UIPost/UIassets.js"
    )
  },
  output: {
    filename: "[name].[contenthash].bundle.js",
    chunkFilename: "[name].[contenthash].bundle.js"
  },
  mode: "production",
  optimization: {
    moduleIds: "hashed",
    runtimeChunk: "single",
    splitChunks: {
      name: false,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  }
});
