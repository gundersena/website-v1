var path = require("path");
/**
 * IndexController
 * @description Bypasss sails routes and use React Router.
 * DO NOT DELETE */

module.exports = {
  render: (req, res) =>
    res.sendFile("index.html", {
      root: path.resolve(__dirname, "../../.tmp/public")
    })
};
