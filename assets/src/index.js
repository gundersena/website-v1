import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "../apple-touch-icon.png";
import "../favicon-16x16.png";
import "../favicon-32x32.png";
import "../safari-pinned-tab.svg";

//promise and iterator polyfills for IE
import "core-js/modules/es.promise";
import "core-js/modules/es.array.iterator";

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
