import React from "react";
import { css } from "@emotion/core";
// First way to import
// import { ClipLoader } from "react-spinners";
// Another way to import. This is recommended to reduce bundle size
import SyncLoader from "react-spinners/SyncLoader";

const override = css`
  display: flex;
  border-color: black;
`;

class LoaderComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  render() {
    return (
      <div className="loader">
        <SyncLoader
          css={override}
          size={10}
          //size={"150px"} this also works
          color={"gray"}
          loading={this.state.loading}
        />
      </div>
    );
  }
}

export default LoaderComponent;
