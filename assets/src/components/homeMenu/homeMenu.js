import React, { useState } from "react";
import Scrollspy from "react-scrollspy";

function HomeMenu() {
  return (
    <nav className="top-menu">
      <Scrollspy items={["home", "featured", "meetOurPlatform", "ourTeam"]} currentClassName="is-current">
        <a href="#home">Home</a>
        <a href="#featured">Featured</a>
        <a href="#meetOurPlatform">Platform</a>
        <a href="#ourTeam">Team</a>

      </Scrollspy>
    </nav>
  );
}

export default HomeMenu;
