import React, { lazy, Suspense } from "react";

import LoaderComponent from "../../loader";

const UIPost = React.lazy(() => {
  return new Promise(resolve => setTimeout(resolve, 1000)).then(() =>
    import("./UIPost")
  );
});

export default function PostPage() {
  return (
    <div>
      <Suspense
        fallback={
          <div className="loader">
            <LoaderComponent />
          </div>
        }
      >
        <UIPost />
      </Suspense>
    </div>
  );
}
