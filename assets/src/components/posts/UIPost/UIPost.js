import React, { Suspense, lazy } from "react";
import ScrollAnimation from "react-animate-on-scroll";
import "./UIPost.scss";

import "./UIassets";

function UIPost() {
  // Ensure page opens at the top
  window.scrollTo(0, 0);

  return (
    <div className="postContainer">
      <div className="uiPost">
        <ScrollAnimation animateIn="fadeIn">
          <section className="part1">
            <div className="animate" animateIn="fadeIn">
              <h1>Artboards for a Photo Sharing App</h1>
              <h2>Motivation:</h2>
              <p>
                For people who enjoy taking photos with their friends and family
                at social events, there is no easy way to share them instantly
                with everyone in the group. By the end of the event, you only
                have a small piece of the total content taken by your group. It
                is an unnecessary hassle to text friends their pictures the next
                day.
              </p>
            </div>
            <img className="animate" src="/images/iphone.svg"></img>
          </section>
        </ScrollAnimation>
        <ScrollAnimation animateIn="fadeIn">
          <section className="part2">
            <div className="animate">
              <h1>Overview</h1>
              <h2>What the app does in a nutshell</h2>
              <p>
                This app allows users to share photos and videos in a group
                automatically. Before or during a social event (a night out with
                friends, a family vacation, a club or business gathering, etc.),
                users can link their phones with the people they want to share
                with by being in the same proximity and pressing a button (logo
                in the middle of the screen). Then, when you take a photo during
                that event, it automatically goes to the people in your group.
                At the end of the event, the session ends, and everyone in the
                group is left with an album of all the content.
              </p>
            </div>
            <span className="animate">
              <img src="/images/connect.svg"></img>
              <img src="/images/camera.svg"></img>
              <img src="/images/editor.svg"></img>
            </span>
          </section>
        </ScrollAnimation>
        <ScrollAnimation animateIn="fadeIn">
          <section className="part3">
            <div className="animate">
              <h1>Layout</h1>
              <h2>There are four main sections of the app, plus messaging.</h2>
              <p>
                The four main sections follow how a user should navigate though
                the app. The first section is the connection screen. When there
                is no session active, the app will automatically open to this
                screen. The connection screen is where users can start a group.
                When the connection is complete, the camera appears into view.
                Slide up on the camera to get to the editor. The editor shows
                all content taken by the group. The large screen at the top of
                the editor is a video that strings all the content together
                automatically, like a Snapchat story. Users can also delete
                content and trim the length of videos. After the event is
                complete, or really at any time in the process, users can pinch
                the screen to access their memories.
              </p>
            </div>
            <span className="animate">
              <img src="/images/connect.svg"></img>
              <img src="/images/camera.svg"></img>
              <img src="/images/editor.svg"></img>
              <img src="/images/memories.svg"></img>
            </span>
          </section>
        </ScrollAnimation>
        <ScrollAnimation animateIn="fadeIn">
          <section className="part4">
            <div className="animate">
              <h1>Messaging + Maps</h1>
              <h2>Circles.</h2>
              <p>
                Sliding left on the camera will bring you to messages and maps.
                Users can see where their friends are in the group. Also, once a
                session begins, a group chat is automatically created with
                people in the group, so that everybody can easily message each
                other right in the app. The group chat ends when the event ends.
                The format of messages lies in stark contrast from most other
                messaging platforms. Long story short, big bubbles are subjects
                and little bubbles are conversations within the subject.
                Messages wrap clockwise around the subjects. Reply by pressing
                on the subject. Or, start a new subject.
              </p>
            </div>
            <span className="animate">
              <img src="/images/messages.svg"></img>
              <img src="/images/map.svg"></img>
            </span>
          </section>
        </ScrollAnimation>
        <ScrollAnimation animateIn="fadeIn">
          <section className="part5">
            <div className="animate">
              <h1>Gallery</h1>
              <h2>Including all of the secondary views</h2>
              <p>
                Here are all the supporting views of the app. Hopefully, this
                gives you a better understanding of how things flow. Like pages
                are grouped together.
              </p>
            </div>
            <span className="animate">
              <img src="/images/loading.svg"></img>
              <img src="/images/ready.svg"></img>
              <img src="/images/connect.svg"></img>
              <img src="/images/begin.svg"></img>

              <img src="/images/camera.svg"></img>
              <img src="/images/edit-group-action.svg"></img>
              <img src="/images/edit-group.svg"></img>
              <img src="/images/end-event.svg"></img>

              <img src="/images/Messages-action.svg"></img>
              <img src="/images/Messages-main.svg"></img>
              <img src="/images/map.svg"></img>
              <img src="/images/Type-message.svg"></img>
              <img src="/images/Messages-scroll.svg"></img>

              <img src="/images/Memories-home.svg"></img>
              <img src="/images/Single-memory-view.svg"></img>
              <img src="/images/Swipe-between-memories.svg"></img>
              <img src="/images/Edit-memories-offline.svg"></img>
              <img src="/images/View-video.svg"></img>
              <img src="/images/View-picture.svg"></img>
              <img src="/images/Picture-options.svg"></img>
              <img src="/images/Burst-options.svg"></img>

              <img src="/images/Editor-action.svg"></img>
              <img src="/images/editor.svg"></img>
              <img src="/images/Edit-video.svg"></img>
              <img src="/images/Edit-memories-offline.svg"></img>
              <img src="/images/View-video.svg"></img>
              <img src="/images/View-picture.svg"></img>
              <img src="/images/Picture-options.svg"></img>
              <img src="/images/Burst-options.svg"></img>

              <img src="/images/camera.svg"></img>
              <img src="/images/Camera-focus.svg"></img>
              <img src="/images/Take-photo-action.svg"></img>
              <img src="/images/Send-content-to-editor.svg"></img>
            </span>
          </section>
        </ScrollAnimation>
      </div>
    </div>
  );
}

export default UIPost;
