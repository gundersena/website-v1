import React from 'react';

import Code from './assets/code'
import AiMessage from './assets/aiMessage'
import SelfMessage from './assets/selfMessage'


function MeetOurPlatformApi() {
  return (
    <div className='contentBlock'>
      <div className='codeBlock'>
        <Code />
      </div>

      <div className='messageBlock'>
        <AiMessage 
          text='Welcome back, Andrew.'
          modifier='Greeting'
        />
        <SelfMessage 
          text='Book a plane ticket to London.'
          modifier='Book plane ticket'
        />
        <AiMessage 
          text='I need a date to book the ticket.'
          modifier='Book plane ticket'
        />
        <SelfMessage 
          text='Hey Sarah should we leave Saturday or Sunday?'
          modifier='Message to Sarah Smith'
        />
        <AiMessage 
          text='Sunday would work best.'
          modifier='Message from Sarah Smith'
        />
        <SelfMessage 
          text='The 14th'
          modifier='Book plane ticket'
        />
        <AiMessage 
          text='Does 3:00PM on June 14th work?'
          modifier='Book plane ticket'
        />
        <SelfMessage 
          text='Yes.'
          modifier='Book plane ticket'
        />
        <AiMessage 
          text='You have a plane ticket with a destination 
of London at 3:00PM on June 14th.'
          modifier='Book plane ticket'
        />
        <AiMessage 
          text='I sent the confirmation to your email. '
          modifier='Book plane ticket'
        />
      </div>

    </div>
  );
}

export default MeetOurPlatformApi;