import React from 'react';

function aiMessage({text, modifier}) {

  return (
    <div className='message'>
      <div className='messageBox'>
        <div className='bubble'>{text}</div>
        <div className='messageMod'>{modifier}</div>
      </div>
    </div>
  );
};

export default aiMessage;