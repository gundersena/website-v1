import React from 'react';

import Bubble from "./bubble"

function CodeChat () {
	return (
    <div className='contentSubBlock'>
      <Bubble 
        color='#c3ecff' 
        text='Welcome back, Andrew.' />
      <Bubble 
        color='#ebebeb' 
        text='Book me a plane ticket to London.' />
      <Bubble 
        color='#ebebeb' 
        text='Book me a plane ticket to London.' />
    </div>
	);
}

export default CodeChat