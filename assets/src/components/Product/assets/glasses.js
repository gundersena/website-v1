import React from 'react'

const Glasses = ({text, color}) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="112.648" height="60.081" viewBox="0 0 112.648 60.081">
    <g id="Group_494" data-name="Group 494" transform="translate(-1021 -1259.919)">
      <g id="Group_493" data-name="Group 493">
        <g id="Rectangle_1251" data-name="Rectangle 1251" transform="translate(1023 1273)" fill="none" stroke={color} stroke-width="5">
          <rect width="42" height="26" rx="7" stroke="none"/>
          <rect x="2.5" y="2.5" width="37" height="21" rx="4.5" fill="none"/>
        </g>
        <g id="Rectangle_1252" data-name="Rectangle 1252" transform="translate(1077 1273)" fill="none" stroke={color} stroke-width="5">
          <rect width="43" height="26" rx="7" stroke="none"/>
          <rect x="2.5" y="2.5" width="38" height="21" rx="4.5" fill="none"/>
        </g>
        <path id="Path_221" data-name="Path 221" d="M898.933-237.341h13.6" transform="translate(165.563 1517.464)" fill="none" stroke={color} stroke-width="3.5"/>
        <path id="Path_222" data-name="Path 222" d="M1144.171-298.537l14.362-12.557" transform="translate(-27.354 1573.482)" fill="none" stroke={color} stroke-linecap="round" stroke-width="3.5"/>
        <path id="Path_223" data-name="Path 223" d="M1144.171-298.537l14.362-12.557" transform="translate(-116.354 1573.482)" fill="none" stroke={color} stroke-linecap="round" stroke-width="3.5"/>
      </g>
      <g id="Group_492" data-name="Group 492" transform="translate(-206)">
        <g id="Rectangle_1253" data-name="Rectangle 1253" transform="translate(1227 1273)" fill="none" stroke="#000" stroke-width="5">
          <rect width="42" height="26" rx="7" stroke="none"/>
          <rect x="2.5" y="2.5" width="37" height="21" rx="4.5" fill="none"/>
        </g>
        <g id="Rectangle_1254" data-name="Rectangle 1254" transform="translate(1281 1273)" fill="none" stroke="#000" stroke-width="5">
          <rect width="43" height="26" rx="7" stroke="none"/>
          <rect x="2.5" y="2.5" width="38" height="21" rx="4.5" fill="none"/>
        </g>
        <path id="Path_224" data-name="Path 224" d="M898.933-237.341h13.6" transform="translate(369.563 1517.464)" fill="none" stroke="#000" stroke-width="3.5"/>
        <path id="Path_225" data-name="Path 225" d="M1144.171-298.537l14.362-12.557" transform="translate(176.646 1573.482)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3.5"/>
        <path id="Path_226" data-name="Path 226" d="M1144.171-298.537l14.362-12.557" transform="translate(87.646 1573.482)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3.5"/>
      </g>
      <text id="David" transform="translate(1067 1317)" font-size="12" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="-18.144" y="0">{text}</tspan></text>
    </g>
  </svg>
)

export default Glasses