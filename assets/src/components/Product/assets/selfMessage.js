import React from 'react';

function selfMessage({text, modifier}) {

  return (
    <div className='selfMessage'>
      <div className='messageBox'>
        <div className='selfbubble'>{text}</div>
        <div className='messageMod'>{modifier}</div>
      </div>
    </div>
  );
};

export default selfMessage;