import React from 'react'

const Arrow = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="35.054" height="14.327" viewBox="0 0 35.054 14.327">
    <g id="Group_491" data-name="Group 491" transform="translate(-754.825 -1320.286)">
      <path id="Path_31" data-name="Path 31" d="M31.09,0H0" transform="translate(787.415 1327.447) rotate(180)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
      <path id="Path_33" data-name="Path 33" d="M7.13,0H0" transform="translate(782.716 1332.492) rotate(-45)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
      <path id="Path_34" data-name="Path 34" d="M7.1,0H0" transform="translate(787.734 1327.426) rotate(-135)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="3"/>
    </g>
  </svg>
)

export default Arrow;