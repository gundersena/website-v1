import React from "react";

import AiIcon from "./assets/aiIcon"
import Glasses from "./assets/glasses"
import SoundWave from "./assets/soundWave"
import Arrow from "./assets/arrow"

function AboutUsSchematic() {

  return (
    <span className='contentBlock' data-tilt>
      <div className='contentSubBlock' id='part1'>
        <div className='contentSubBlockLine'>
          Hey Anna, are you going to the chess convention?
        </div>
        <SoundWave className='contentSubBlockLine' />
        <div className='schematicDesc'>
          User A talks to their friend Anna.
        </div>
      </div>
      <Arrow className='arrow' id='part2'/>
      <div className='contentSubBlock' id='part3'>
        <AiIcon />
        <div className='schematicDesc'>
          Crimata AI
        </div>
      </div>
      <Arrow className='arrow' id='part4'/>
      <div className='contentSubBlock' id='part5'>
        <Glasses text={'David'} />
        <Glasses text={'Anna'} color={"#fd1db9"} />
        <Glasses text={'Andrew'}/>
        <div className='schematicDesc'>
          Anna receives message.
        </div>
      </div>
    </span>
  );
}

export default AboutUsSchematic;