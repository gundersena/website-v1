import React from "react"; 

import MeetOurPlatformApi from './meetOurPlatformApi';
import IntentSchematic from './intentSchematic';

function OurPlatformPage() {
  return (
    <section className="meetOurPlatform" id="meetOurPlatform">
      <h1 className='colorTitle'>Our Platform</h1>

      <p className='colorTitleSub'>Implicit message intent classification enables you to talk to your friends directly</p>
      <IntentSchematic />

      <h2>Developer Kit</h2>
      <p>A hello world application demonstrates the power Crimata.</p>

      <MeetOurPlatformApi />

      <div className="videoBlock">
        <iframe frameBorder="0"
        allowfullscreen="allowfullscreen"
        mozallowfullscreen="mozallowfullscreen" 
        msallowfullscreen="msallowfullscreen" 
        oallowfullscreen="oallowfullscreen" 
        webkitallowfullscreen="webkitallowfullscreen"
        
          src="https://www.youtube.com/embed/O_7tKN_CN1Q">
        </iframe>
      </div>
      
    </section>
  );
}

export default OurPlatformPage;