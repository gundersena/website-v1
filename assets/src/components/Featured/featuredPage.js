import React from "react";

import "../../../images/appMoc.svg";


function FeaturedPage() {

  return (
    <section className="featured" id="featured">
      <h1>Featured</h1>
      <p>Crimata for Mac (Limited Beta)</p>

      <object className='appMoc' data="./images/appMoc.svg" type="image/svg+xml" />
    </section>
  );
}

export default FeaturedPage;
