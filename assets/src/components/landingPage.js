import React from "react";

// import componants
import HomeMenu from "./homeMenu/homeMenu.js";
import HomePage from "./home/homePage";
import FeaturedPage from "./Featured/featuredPage";
import MeetOurPlatformPage from "./Product/meetOurPlatformPage";
import OurTeamPage from "./Team/ourTeamPage";
import Footer from "./home/footerPage";

// import style sheets
import "animate.css/animate.min.css";
import "../../styles/main.scss";

function LandingPage() {
  return (
    <div>
      <HomeMenu />
      <HomePage />
      <div className="content">
        <FeaturedPage />
        <MeetOurPlatformPage />
        <OurTeamPage />
        <Footer />
      </div>
    </div>
  );
}

export default LandingPage;
