import React from "react";
import classNames from 'classnames/bind';

// import assets
import "./assets/ourTeam-andrewProfile.png";
import "./assets/ourTeam-enriqueProfile.png";
import "./assets/ourTeam-landonProfile.png"

import ProfileCard from './profileCard'



var profileClasses = classNames('profiles', 'tablet', 'desktop');

function OurTeamPage() {
  return (
    <section className="ourTeam" id="ourTeam">
      <h1>Our Team</h1>
      <p>
        Crimata was founded by Xavier University Physics and CS alumni
      </p>

      <div className='profileDiv'>
        <ProfileCard 
          name='Andrew Gundersen' 
          img='./images/ourTeam-andrewProfile.png' 
          title='Co-Founder and CEO' 
          url1='https://www.linkedin.com/in/gundersena/'/>
        <ProfileCard 
          name='Enrique Hernandez' 
          img='./images/ourTeam-enriqueProfile.png' 
          title='Co-Founder'
          url1='https://www.linkedin.com/in/hernandeze/'/>
      </div>





      {/* <object className='desktop' data='./images/ourTeam-icons.svg' type="image/svg+xml" />
      <object className='tablet' data='./images/ourTeam-icons.svg' type="image/svg+xml" />
      <object className='mobile' data='./images/ourTeam-iconsMobile.svg' type="image/svg+xml" />
      <section className={profileClasses}> 
        <section className='profile'> 
          <img src="./images/ourTeam-andrewProfile.svg"></img>
          <section className="details">
            <section className="name">Andrew Gundersen</section>
            <section className="icons">
              <a href="https://www.linkedin.com/in/gundersena/" target="_blank">
                <img src="./images/ourTeam-linkedinIcon.svg"></img>
              </a>
              <a href="https://www.instagram.com/andrew.gundy/" target="_blank">
                <img src="./images/ourTeam-instagramIcon.svg"></img>
              </a>
            </section>
            <section className="job">Co-Founder and CEO</section>
          </section>
        </section>
        <section className="profile">
          <img src="./images/ourTeam-enriqueProfile.svg"></img>
          <section className="details">
            <section className="name">Enrique Hernandez</section>
            <section className="icons">
              <a href="https://www.linkedin.com/in/hernandeze/" target="_blank">
                <img src="./images/ourTeam-linkedinIcon.svg"></img>
              </a>
              <a href="https://www.instagram.com/riquenandez/" target="_blank">
                <img src="./images/ourTeam-instagramIcon.svg"></img>
              </a>
            </section>
            <section className="job">Co-Founder</section>
          </section>
        </section>
      </section>
      <object className='mobile' data='./images/ourTeam-profilesMobile.svg' type="image/svg+xml" />
      <h1 className="accomplishments">Projects</h1>
      <p className="description">
        Over their academic careers, Andrew and Enrique lead multiple
        engineering projects, some of which are included below.
      </p>
      <section className="cards">
        <object className='desktop'
          data="/images/ourTeam-aiPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='tablet'
          data="/images/ourTeam-aiPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='mobile'
          data="/images/ourTeam-aiPostThumbnailMobile.svg"
          type="image/svg+xml"
        />


        <object className='desktop'
          data="/images/ourTeam-hubPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='tablet'
          data="/images/ourTeam-hubPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='mobile'
          data="/images/ourTeam-hubPostThumbnailMobile.svg"
          type="image/svg+xml"
        />


        <object className='desktop'
          data="/images/ourTeam-pcbPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='tablet'
          data="/images/ourTeam-pcbPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='mobile'
          data="/images/ourTeam-pcbPostThumbnailMobile.svg"
          type="image/svg+xml"
        />

        
        <object className='desktop'
          data="/images/ourTeam-uiPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='tablet'
          data="/images/ourTeam-uiPostThumbnail.svg"
          type="image/svg+xml"
        />
        <object className='mobile'
          data="/images/ourTeam-uiPostThumbnailMobile.svg"
          type="image/svg+xml"
        />
      </section> */}
    </section>
  );
}

export default OurTeamPage;
