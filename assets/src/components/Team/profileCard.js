import React from 'react';

import './assets/instaIcon.svg'
import './assets/linkedInIcon.svg'

function ProfileCard({name, img, title, url1, url2}) {
  return (
    <div className='teamProfile'>
      <div className='contentSubBlock'>
        <div className='contentSubBlockLine'>
          {name}
        </div>
        <div className='contentSubBlockLine'>
          <img className='Tilt-inner' src={img}></img>
        </div>
        <div className='contentSubBlockLine'>
          {title}
        </div>
      </div>
      <div className='socialLinks'>
        <a href={url1} target="_blank">
          <img className='Tilt-inner' src='./images/linkedInIcon.svg'></img>
        </a>
      </div>
    </div>
  );
}

export default ProfileCard;