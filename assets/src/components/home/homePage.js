import React from "react";
import ScrollAnimation from "react-animate-on-scroll";

import "../../../images/title.svg";
import "../../../images/arrow.svg";
import "../../../images/mission.pdf";

function HomePage() {
  return (
    <section className="home" id="home">
      <section className="contact">
        <a href="./images/mission.pdf" target="_blank">Mission</a>
        <a href="mailto:gundersena@crimata.com">Contact Us</a>
      </section>
      <ScrollAnimation animateIn="fadeInDown">
        <header className='title'>
            <object data="./images/title.svg" type="image/svg+xml" />
            <h2>A platform for writing voice-driven applications</h2>
        </header>
      </ScrollAnimation>
      <footer>
        <a href="#aboutUs">
          <img src="./images/arrow.svg" alt="scroll down" />
        </a>
      </footer>
    </section>
  );
}

export default HomePage;