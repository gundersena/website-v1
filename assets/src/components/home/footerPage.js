import React from 'react'

// import assets
import '../../../images/footer.svg'

function Footer() {
  return (
    <section className='footer'>
      <section className='letterhead'>
        Crimata Technologies 2021
      </section>
      <section className='footer-buttons'>
        <a href="#home">Home</a>
        <a href="#featured">Featured</a>
        <a href="#meetOurPlatform">Platform</a>
        <a href="#ourTeam">Team</a>
        <a href="mailto:gundersena@crimata.com">Contact</a>
      </section>
    </section>
  )
}

export default Footer;