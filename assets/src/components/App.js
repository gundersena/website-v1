import React, { Suspense, lazy } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

// import supporting componants
import ErrorBoundary from "./errorBoundary";
import LoaderComponent from "./loader";

// import main componants
// const LandingPage = lazy(() => import("./landingPage"));
// const AIPost = lazy(() => import("./posts/AIPost/AIpost"));
// const HubPost = lazy(() => import("./posts/HubPost/HubPost"));
// const PCBPost = lazy(() => import("./posts/PCBPost/PCBPost"));

const LandingPage = lazy(()=>{
    return import("./landingPage")
})
const AIPost = lazy(()=>{
    return import("./posts/AIPost/AIpost")
})
const HubPost = lazy(()=>{
    return import("./posts/HubPost/HubPost")
})

const PCBPost = lazy(()=>{
    return import("./posts/PCBPost/PCBPost")
})
const Post = lazy(() => {
    return import("./posts/UIPost/postPage")
});

function App() {
  return (
    <BrowserRouter>
      <ErrorBoundary>
        <Suspense
          fallback={
            <div className="loader">
              <LoaderComponent />
            </div>
          }
        >
          <Switch>
            <Route path="/" exact component={LandingPage} />
            <Route path="/AIPost" component={AIPost} />
            <Route path="/HubPost" component={HubPost} />
            <Route path="/PCBPost" component={PCBPost} />
            <Route path="/UIPost" component={Post} />
            <Route path="/" render={() => <div>404</div>} />
          </Switch>
        </Suspense>
      </ErrorBoundary>
    </BrowserRouter>
  );
}

export default App;
