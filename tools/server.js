const config = require("../webpack.dev");
const path = require("path");
const webpack = require("webpack");
const opn = require("opn");
const WebpackDevServer = require("webpack-dev-server");

const compiler = webpack(config);

const serverConfig = {
  contentBase: path.resolve(__dirname, "../assets"),
  hot: true,
  publicPath: config.output.publicPath,
  historyApiFallback: true,
  proxy: {
    "/api/*": "http://localhost:1337"
  },
  headers: {
    "Cache-Control": "no-cache, no-store, must-revalidate",
    Pragma: "no-cache",
    Expires: "-1",
    "X-Frame-Options": "SAMEORIGIN"
  }
};

const server = new WebpackDevServer(compiler, serverConfig);

server.use(require("webpack-hot-middleware")(compiler));

const host = "localhost";
const port = 8080;

server.listen(port, host, err => {
  if (err) {
    console.log("[webpack-dev-server] failed to start: ", err);
  } else {
    const suffix = config.output.publicPath;
    opn(`http://localhost:${port}`);
  }
});

server.app.get("/reload", (req, res) => {
  //tell connected browsers to reload.
  server.sockWrite(server.sockets, "ok");
  res.sendStatus(300);
});

server.app.get("/invalid", (req, res) => {
  //tell connected browsers some change is about to happen.
  server.sockWrite(server.sockets, "invalid");
  res.sendStatus(200);
});
