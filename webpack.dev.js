const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const merge = require("webpack-merge");
const config = require("./webpack.config");

module.exports = merge(config, {
  mode: "development",
  output: {
    filename: "[name].[hash].bundle.js",
    chunkFilename: "[name].[hash].bundle.js"
  },
  devtool: "inline-source-map",
  entry: ["./assets/src/index.js", "webpack-hot-middleware/client?reload=true"],
  plugins: [
    new HtmlWebpackPlugin({
      template: "./assets/index.html",
      title: "Crimata (DevMode)"
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
});
