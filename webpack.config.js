const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const precss = require("precss");
const autoprefixer = require("autoprefixer");
const path = require("path");

module.exports = {
  entry: "./assets/src/index.js",

  output: {
    path: path.join(__dirname, "/.tmp/public"),
    publicPath: "/"
  },
  module: {
    rules: [
      {
        use: "babel-loader",
        test: /\.(js|jsx)$/,
        exclude: /node_modules/
      },
      {
        use: ["style-loader", "css-loader"],
        test: /\.css$/
      },
      {
        test: /\.(scss)$/,
        use: [
          {
            loader: "style-loader" // inject CSS to page
          },
          {
            loader: "css-loader" // translates CSS into CommonJS modules
          },
          {
            loader: "postcss-loader", // Run post css actions
            options: {
              plugins: () =>
                // post css plugins, can be exported to postcss.config.js
                [precss, autoprefixer]
            }
          },
          {
            loader: "sass-loader" // compiles Sass to CSS
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loader: "url-loader",
        options: {
          limit: 10 * 24
        }
      },
      {
        test: /\.svg$/,
        loader: "svg-url-loader",
        options: {
          limit: 10 * 24,
          noquotes: true
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg|pdf)$/,
        loader: "file-loader",
        // Specify enforce: 'pre' to apply the loader
        // before url-loader/svg-url-loader
        // and not duplicate it in rules with them
        enforce: "pre",
        options: {
          name: "[name].[ext]",
          outputPath: "./images",
          publicPath: "/"
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "./fonts"
            }
          }
        ]
      },
      {
        test: /\.(m4a|mp4)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "./media",
          publicPath: "/"
        }
      },
      {
        test: /\.(zip)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "./downloads",
          publicPath: "/"
        }
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: "Crimata",
      template: "./assets/index.html",
      favicon: "./assets/favicon.ico"
    })
  ]
};
